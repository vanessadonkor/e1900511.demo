package fi.vamk.e1900511.demo;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@NamedQuery(name="Attendance.findAll", query = "SELECT p FROM Attendance p")
public class Attendance implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String key;
	private String date;
	
	public Attendance() {}
	
	public Attendance(String key, String date) {
		this.key = key;
		this.date = date;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String toString() {
		return id + " " + key + " " + date;
	}
}
