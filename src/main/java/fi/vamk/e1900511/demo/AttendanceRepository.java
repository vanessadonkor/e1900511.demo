package fi.vamk.e1900511.demo;

import org.springframework.data.repository.CrudRepository;

public interface AttendanceRepository extends CrudRepository<Attendance, Integer> {
	public Attendance findByKey(String key);
	public Attendance findByDate(String date);

}
